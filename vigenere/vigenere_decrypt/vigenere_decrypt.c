#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <my_lib.h>

int main(int argc, char** argv)
{
  char* key = NULL;

  if(argc != 2)
    {
      fprintf(stderr, "E : bad argument number !\n");
      fprintf(stderr, "\tUsage : vigenere_decrypt <key>\n");
      exit(EXIT_FAILURE);
    }

  key = argv[1];

  FILE* file;
  char const* file_name = "vigenere_decrypt_in.txt";
  file = fopen(file_name, "a+");
  
  if( file == NULL )
    {
      fprintf(stderr, "E : can't read %s", file_name);
      exit(EXIT_FAILURE);
    }
  
  FILE* dest = stdout;
  main_loop(&file, &dest, &vigenere_decrypt,key);

  fclose(file);
  
  return 0;
}
