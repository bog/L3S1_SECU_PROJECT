#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <my_lib.h>

int main(int argc, char** argv)
{
  char* key = NULL;

  if(argc != 2)
    {
      fprintf(stderr, "E : bad argument number !\n");
      fprintf(stderr, "\tUsage : vigenere_encrypt <key>\n");
      exit(EXIT_FAILURE);
    }

  key = argv[1];
  FILE* file;
  file = fopen("vigenere_encrypt_in.txt", "a+");

  if( file == NULL )
    {
      perror("Can't load input file");
      exit(EXIT_FAILURE);
    }

  FILE* output = stdout;
  main_loop(&file, &output, &vigenere_encrypt, key);

  fclose(file);
  return 0;
}
