cmake_minimum_required(VERSION 2.6)

project(substitution_break)

file(GLOB
  substitution_break_src
  *.c
  )

add_executable(substitution_break.elf
  ${substitution_break_src}
  )

target_link_libraries(substitution_break.elf
  mylib
  )
