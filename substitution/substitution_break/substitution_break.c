// EXPORT pour sauvegarder la buffer et non pas la substitution

#include <stdlib.h>
#include <stdio.h>
#include <global.h>
#include <my_lib.h>

int main(int argc, char** argv)
{
  FILE* file;
  file = fopen("substitution_break_in.txt", "a+");

  if( file == NULL )
    {
      fprintf(stderr, "E : Can't open the input file !");
      exit(EXIT_FAILURE);
    }

  // original text
  char text[FILE_SIZE_MAX];
  fread(text, sizeof(char), FILE_SIZE_MAX, file);
  size_t const size = strlen(text);
  fclose(file);

  substitution_t sub;
  
  // buffer text
  char buffer[FILE_SIZE_MAX];
  memcpy(buffer, text, sizeof(text));
  
  int action = 0;
  
  int letters_is_original[ALPHA_NUM];
  for(size_t i=0; i<ALPHA_NUM; i++)
    {
      letters_is_original[i]=0;
      sub.letters[i] = ' ';
    }
  
  statistic_t stat;
  init_stat(&stat);
  make_stat_from_str(&stat, text, strlen(text));
  
  substitution_print_menu();

  while( action != E_SUBSTITUTION_QUIT )
    {      
      action = substitution_get_action();
      substitution_make_action(&sub, &stat, text, buffer, size, action, (int*)&letters_is_original);      

    }
  
  return 0;
}

