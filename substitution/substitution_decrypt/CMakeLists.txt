cmake_minimum_required(VERSION 2.6)

project(substitution_decrypt)

file(GLOB
  substitution_decrypt_src
  *.c
  )

add_executable(substitution_decrypt.elf
  ${substitution_decrypt_src}
  )

target_link_libraries(substitution_decrypt.elf
  mylib
  )
