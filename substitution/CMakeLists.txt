cmake_minimum_required(VERSION 2.6)

project(substitution)

add_subdirectory(substitution_break)
add_subdirectory(substitution_decrypt)
add_subdirectory(substitution_encrypt)
