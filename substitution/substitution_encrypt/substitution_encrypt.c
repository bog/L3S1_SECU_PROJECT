#include <stdlib.h>
#include <stdio.h>
#include <global.h>
#include <my_lib.h>

int main(int argc, char** argv)
{
  if( argc !=2 )
    {
      fprintf(stderr, "E : Bad number of arguments !");
      fprintf(stderr,"\n\tUsage : substitution_encrypt <key>\n");
      exit(EXIT_FAILURE);
    }

  char *key = argv[1];
  size_t key_size = strlen(key);
  
  substitution_t subs;
  substitution_init(&subs);
  substitution_fill_with_key(&subs, key, key_size);

  FILE* file = fopen("substitution_encrypt_in.txt", "a+");
  
  if( file == NULL )
    {
      fprintf(stderr, "E : can't open input file !\n");
      exit(EXIT_FAILURE);
    }

  FILE* output = stdout;
  
  main_loop(&file, &output, &substitution_encrypt, &subs);
  
  fclose(file);
  return 0;
}
