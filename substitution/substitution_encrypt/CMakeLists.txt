cmake_minimum_required(VERSION 2.6)

project(substitution_encrypt)

file(GLOB
  substitution_encrypt_src
  *.c
  )

add_executable(substitution_encrypt.elf
  ${substitution_encrypt_src}
  )

target_link_libraries(substitution_encrypt.elf
  mylib
  )
