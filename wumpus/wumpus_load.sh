#!/bin/bash

/sbin/touch "caesar_break_in.txt"
/sbin/touch "caesar_encrypt_in.txt"
/sbin/touch "caesar_decrypt_in.txt"
/sbin/touch "vigenere_break_in.txt"
/sbin/touch "vigenere_encrypt_in.txt"
/sbin/touch "vigenere_decrypt_in.txt"
/sbin/touch "substitution_break_in.txt"
/sbin/touch "substitution_encrypt_in.txt"
/sbin/touch "substitution_decrypt_in.txt"

### WUMPUS MONSTER ###
echo -e "\e[35mWumpus - \e[31mI am so hungry !!!"
echo -e "\e[0m"

# Eat the temporary files
rm -f *~

# Look for food
PATH=$(/sbin/find  . -name "*_in*.txt")
FILE=$(/sbin/find . -name "*txt*.txt")

if [ "$FILE" == "" ]
then
    echo -e "\e[35mWumpus - \e[31mThere's nothing to eat è_é"
    echo -e "\e[0m"
    exit 1
else
    echo -e "\e[35mWumpus - \e[34mI could eat $FILE !"
    echo -e "\e[0m"
fi

# Eat the text to decrypt
for input_file in $PATH
do
    /sbin/cp -fv $FILE $input_file
done

/sbin/rm -fv $FILE

# Thanks to feed the wumpus !
echo -e "\e[35mWumpus : \e[32mYum yum, it was pretty good !"
echo -e "\e[0m"

exit 0
