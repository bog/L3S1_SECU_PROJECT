#include <stdlib.h>
#include <stdio.h>
#include <global.h>
#include <my_lib.h>

int main(int argc, char** argv)
{
  int offset = 0;

  if( argc != 2 )
    {
      fprintf(stderr, "E : bad arg number\n\tUsage : caesar-decrypt offset\n");
      return EXIT_FAILURE;
    }
  else
    {
      offset = atoi(argv[1]);
    }
  
  if( offset < 0 ){ offset += ALPHA_NUM; }

  FILE* file;
  file = fopen("caesar_decrypt_in.txt", "a+");

  if(file == NULL)
    {
      perror("Can't open input file");
      exit(EXIT_FAILURE);
    }

  FILE* output = stdout;
  main_loop(&file, &output, &caesar_decrypt, &offset);

  fclose(file);
  return 0;
}
