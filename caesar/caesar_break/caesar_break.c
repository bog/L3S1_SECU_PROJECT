#include <stdlib.h>
#include <stdio.h>
#include <global.h>
#include <my_lib.h>

void write_file(char const* name, statistic_t* stat);
void read_file(char const* name, int offset);

int main(int argc, char** argv)
{
  init();
  statistic_t stat;
  init_stat(&stat);
  
  //READING FILE
  write_file("break.tmp", &stat);

  char c = find_e_stat(&stat);
  printf("----\n");

  read_file("break.tmp", ('E' - c));

  int brut_force = 0;
  
  {
    
    printf("Brut force mode ? (0/1)?\n");
    scanf("%d", &brut_force);
  }

  if(brut_force)
    {
      for(size_t i=0; i<ALPHA_NUM; i++)
	{
	  printf("----\n");
	  read_file("break.tmp", (i - c));
	}
    }
  
  return 0;
}

void read_file(char const* name, int offset)
{
  FILE* file = fopen(name, "r");
  
  if( !file )
    {
      fprintf(stderr, "E : can't read %s !\n", name);
      return EXIT_FAILURE;
    }
  
  size_t sz = 0;
  char buf;

  printf("Offset : %d\n", abs(offset)%ALPHA_NUM);
  
  while( (sz=fread(&buf, sizeof(char), 1, file)) )
    {
      char c = buf;
      
      if(c >= 'A' && c <= 'Z')
	{
	  if( offset > 0 ){ c = rotate_right(buf, offset); }
	  else { c = rotate_left(buf, -offset); }
	}
	
      
      fwrite(&c, sizeof(char), 1, stdout);
    }
  
  fclose(file);
}

void write_file(char const* name, statistic_t* stat)
{
  FILE *file = fopen(name, "w+");
  
  if( !file )
    {
      fprintf(stderr, "E : can't write into %s !\n", name);
      return EXIT_FAILURE;
    }
  
  int i;

  while ((i = fgetc(stdin)) != EOF)
    {
      unsigned char m = (unsigned char) i;
      
      if( m == ' ' || m == '\n'
	  || (m >= 'A' && m <= 'Z'))
	{
	  fwrite(&m, sizeof(unsigned char), 1, file);

	  if( m >= 'A' && m <= 'Z' )
	    {
	      stat->letter_count[m - 'A']++;
	      stat->letter_total++;
	    }
	}
    }

  fclose(file);
}


// Text used for testing
/*
  JE PENSE DONC JE SUIS
 */

/* ceasar 3
   MH SHQVH GRQF MH VXLV
 */
