#ifndef MY_LIB_H
#define MY_LIB_H
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <global.h>

#define FILE_SIZE_MAX 409600

// Possibles action in substitution break menu
enum
  {
    // continue using the interface (do nothing more)
    E_SUBSTITUTION_CONTINUE,
    // print the original text
    E_SUBSTITUTION_ORIGINAL,
    // print the modified text
    E_SUBSTITUTION_PRINT,
    // print the substition table
    E_SUBSTITUTION_TABLE,
    // print the substitution inverted table,
    // can be useful to guess the key
    E_SUBSTITUTION_INVERTED_TABLE,
    // print the original text statistics
    E_SUBSTITUTION_STAT,
    // remove all the modification
    // undo all modification
    E_SUBSTITUTION_DELETE,
    // guess some letters automatically
    // actually it only find the e, the q and the u
    E_SUBSTITUTION_WIZARD,
    // swap the first letter with the second in the modified text
    // example : rAB change all the A into B
    E_SUBSTITUTION_SWAP,
    // remove all the changes made on one letter
    // example : rABkA change the A into a B and then undo the operations one
    // A (A become A again)
    E_SUBSTITUTION_KILL,
    // Print the menu
    E_SUBSTITUTION_HELP,
    // Save the substituion table
    E_SUBSTITUTION_SAVE,
    // Load the substitution table
    E_SUBSTITUTION_LOAD,
    // Export the modified text
    E_SUBSTITUTION_EXPORT,
    // Quit the substituion_break menu
    E_SUBSTITUTION_QUIT
  };

// Most used digrammes
enum
  {
    ES,
    LE,
    EN,
    DE,
    RE,
    NT,
    ON,
    TE,
    ER,
    SE,
    DIGRAMME_NUM
  };

// a digramme is a group of two letters
typedef struct
{
  char letter1;
  char letter2;
  size_t count;

} digramme_t;

// used for sort the keys 
typedef struct
{
  size_t size;
  float ioc;
} key_size_t;

typedef struct
{
  char letters[ALPHA_NUM];
  char *key;
  size_t key_size;
  
} substitution_t;

extern digramme_t digrammes_g[DIGRAMME_NUM];

typedef struct
{
  int letter_count[ALPHA_NUM];
  float letter_average_offset[ALPHA_NUM];
  digramme_t digrammes[ALPHA_NUM * ALPHA_NUM];
  size_t letter_total;
  
} statistic_t;

// Initial functions
void init();

// Statsitics
void dump_stat(statistic_t* stat);
void init_stat(statistic_t* stat);

// find the letter e using statistics
char find_e_stat(statistic_t* stat);
char find_e_digramme_stat(statistic_t* stat, size_t digramme_type, int* occ);

void make_stat_from_str(statistic_t* stat, char* str, size_t size);
size_t count_char_stat(statistic_t* stat, char letter);

// Common encryption/decryption utilities
char rotate_left(char letter, int offset);
char rotate_right(char letter, int offset);

float index_of_coicidence(char* str, size_t size);
int fequals(float lvalue, float rvalue, float approx);
float fapprox(float lvalue, float rvalue);

// Convenients functions
void main_loop(FILE** src, FILE** dest, char (*do_something) (char, void*, int), void* data);
void main_loop_stdio(char (*do_something) (char, void*, int), void* data);
char do_nothing(char letter, void* data, int inc);

char substitution_encrypt(char letter, void* subs, int itr);
char substitution_decrypt(char letter, void* subs, int itr);
char caesar_encrypt (char letter, void* data, int itr);
char caesar_decrypt(char letter, void* data, int itr);
char vigenere_encrypt(char letter, void* data, int inc);
char vigenere_decrypt(char letter, void* data, int inc);

// Utilities used to find the key
// take the string str and keep one letter out of step
void mask_string(char* str, size_t size, size_t step);

// remove all the space in str
void mask_space(char* str, size_t size);
void vigenere_edit_key(char* key, size_t key_size, char* str);

// try to guess the key using statistics
void get_a_key(char* str, char* key, size_t const key_size);
int key_size_compare_ioc(void const* k1, void const* k2);
// fill the array keys with more probable key sizes
void get_key_size(key_size_t* keys, char* str, size_t const size);

// Digrammes
void init_digrammes_g();
int digramme_compare(const void* d1, const void* d2);

// Substitution utilities
// keep only one occurency of each characters (used for substitution)
void make_char_unique(char* str, size_t size);
void substitution_init(substitution_t* self);
void substitution_fill_with_key(substitution_t* self, char* key, size_t key_size);
void substitution_dump(substitution_t *self);
void substitution_i_dump(substitution_t *self);

// Display utilities
void get_string(char* str, size_t size);
void print_colored_key(char* key, size_t key_size);

// Partial decryption utilities
void partial_vigenere_decrypt(char* str, char* key, size_t n);
void partial_vigenere_decrypt_color(char* str, char* key, size_t n, int colored);

// Substitution
// for menu description, look at the enum at the begining of this file
void substitution_print_menu();
int substitution_get_action();
void substitution_wizard(substitution_t* sub, statistic_t* stat, char* text, int* letters_is_original);
void substitution_swap(substitution_t* sub, char letter1, char letter2, int* letters_is_original);
void substitution_apply(substitution_t* sub, char* str, size_t size);
void substitution_get_swap(substitution_t* sub, int* letters_is_original);
void substitution_get_kill(substitution_t* sub, int* letters_is_original);
void substitution_kill(substitution_t* sub, char l, int* letters_is_original);
void substitution_make_action(substitution_t* sub, statistic_t* stat, char* text, char* buffer, size_t size, int action, int* letters_is_original);

#endif // MY_LIB_H
