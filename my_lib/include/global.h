#ifndef GLOBAL_H
#define GLOBAL_H

#define ALPHA_NUM 26
#define KEY_MAX 256
#define DIGRAMME_THRES 2
#define E_THRES 2
#define E_AVERAGE_OFFSET 6.892933
#define OCC_DELTA_THRES 10
#define FRENCH_IOC 0.0778
#define KEY_SIZE_MIN 5
#define KEY_SIZE_MAX 15

#endif // GLOBAL_H
