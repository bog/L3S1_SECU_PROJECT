#include <my_lib.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <time.h>

digramme_t digrammes_g[DIGRAMME_NUM];

void init()
{
  init_digrammes_g();
}

void init_digrammes_g()
{
  digrammes_g[ES].letter1 = 'E';
  digrammes_g[ES].letter2 = 'S';
  digrammes_g[LE].letter1 = 'L';
  digrammes_g[LE].letter2 = 'E';
  digrammes_g[EN].letter1 = 'E';
  digrammes_g[EN].letter2 = 'N';
  digrammes_g[DE].letter1 = 'D';
  digrammes_g[DE].letter2 = 'E';
  digrammes_g[RE].letter1 = 'R';
  digrammes_g[RE].letter2 = 'E';
  digrammes_g[NT].letter1 = 'N';
  digrammes_g[NT].letter2 = 'T';
  digrammes_g[ON].letter1 = 'O';
  digrammes_g[ON].letter2 = 'N';
  digrammes_g[TE].letter1 = 'T';
  digrammes_g[TE].letter2 = 'E';
  digrammes_g[ER].letter1 = 'E';
  digrammes_g[ER].letter2 = 'R';
  digrammes_g[SE].letter1 = 'S';
  digrammes_g[SE].letter2 = 'E';

  for(size_t i=0; i<DIGRAMME_NUM; i++)
    {
      digrammes_g[i].count = 0;
    }
}

char rotate_right(char letter, int offset)
{
  if( letter < 'A' || letter > 'Z' ){ return letter; }
  
  while(offset < 0 ){ offset += ALPHA_NUM; }

  return (char)( ( ( (int)(letter - 'A') + offset ) % (ALPHA_NUM) ) + 'A');
}

char rotate_left(char letter, int offset)
{
  return rotate_right(letter, -offset );
}


void init_stat(statistic_t* stat)
{
  assert( stat );
  
  for(size_t i=0; i<ALPHA_NUM*ALPHA_NUM; i++)
    {
      if( i < ALPHA_NUM )
	{
	  stat->letter_count[i] = 0;
	}
      
      size_t l1 = i/ALPHA_NUM;
      size_t l2 = i%ALPHA_NUM;
      
      stat->digrammes[i].letter1 = 'A' + l1;
      stat->digrammes[i].letter2 = 'A' + l2;
      stat->digrammes[i].count =  0;
    }

  stat->letter_total = 0;
}

void dump_stat(statistic_t* stat)
{
  assert( stat );
    
  printf("Statistics\n");
  
  for(size_t i = 0; i<ALPHA_NUM; i++)
    {
      printf("\t%c : %d\n"
	     , (char)(i + 'A')
	     , stat->letter_count[i]);

    }
  
  printf("\n");
  
  for(size_t i = 0; i<ALPHA_NUM*ALPHA_NUM; i++)
    {
      printf("%c%c[%zd] "
  	     , stat->digrammes[i].letter1
  	     , stat->digrammes[i].letter2
  	     , stat->digrammes[i].count);
    }
  printf("\n");
  
  printf("\nTotal : %zd\n", stat->letter_total);
  
}

void make_stat_from_str(statistic_t* stat, char* str, size_t size)
{
  assert(stat);

  // letters
  for(size_t i = 0; i<size; i++)
    {
      char letter = str[i];
      
      if(letter >= 'A' && letter <= 'Z')
	{
	  stat->letter_count[(size_t)(letter - 'A')]++;
	  stat->letter_total++;
	}
      
      if( i == size - 1 ){ continue; }
      
      // digrammes
      {
	char letter1 = letter;
	char letter2 = str[i+1];
	
	if( letter1 >= 'A' && letter1 <= 'Z'
	    && letter2 >= 'A' && letter2 <= 'Z')
	  {
	    size_t l1 = abs(letter1 - 'A');
	    size_t l2 = abs(letter2 - 'A');
	    size_t dig_index = l2 + l1 * ALPHA_NUM;
	  
	    stat->digrammes[dig_index].count++;
	  }
      }
    }

  qsort(stat->digrammes, ALPHA_NUM*ALPHA_NUM, sizeof(digramme_t), &digramme_compare);
}

int digramme_compare(const void* d1, const void* d2)
{
  digramme_t* digramme1 = (digramme_t*)(d1);
  digramme_t* digramme2 = (digramme_t*)(d2);

  return digramme1->count < digramme2->count;
}


char find_e_digramme_stat(statistic_t* stat, size_t digramme_type, int* occ)
{
  size_t es_index = 0;
  
  while ( stat->digrammes[es_index].letter1 == stat->digrammes[es_index].letter2
  	  || stat->digrammes[es_index].letter1 - stat->digrammes[es_index].letter2 !=
  	  digrammes_g[digramme_type].letter1 - digrammes_g[digramme_type].letter2)
    {
      es_index++;
    }
  
  es_index += digramme_type;
  
  if( occ != NULL ) { *occ =  stat->digrammes[es_index].count; }
  
  
  return stat->digrammes[es_index].letter1;
}

  
char find_e_stat(statistic_t* stat)
{
  assert(stat);
  
  // find the letter with the more occurencies
  size_t index_letter_max = 0;
  
  for(size_t i = 0; i<ALPHA_NUM; i++)
    {
      if( stat->letter_count[i] > stat->letter_count[index_letter_max] )
	{
	  index_letter_max = i;
	}
    }

  size_t index_2nd_letter_max = 0;
  {
    stat->letter_count[index_letter_max] *= -1;
  
    for(size_t i = 0; i<ALPHA_NUM; i++)
      {
  	if( stat->letter_count[i] > stat->letter_count[index_2nd_letter_max] )
  	  {
  	    index_2nd_letter_max = i;
  	  }
      }
    stat->letter_count[index_letter_max] *= -1;
  }

  return index_letter_max + 'A';

}

void main_loop(FILE** src, FILE** dest, char (*do_something) (char, void*, int), void* data)
{
  int input = 0;
  int i = 0;
  
  while ( (input = fgetc(*src)) != EOF )
    {
      unsigned char letter = (unsigned char) input;

      if( letter >= 'A' && letter <= 'Z' )
	{
	  char encrypted = do_something(letter, data, i);
	  fwrite(&encrypted, sizeof(char), 1, *dest);
	  i++;
	}
      else
	{
	  fwrite(&letter, sizeof(char), 1, *dest);
	}
    }
}

void main_loop_stdio(char (*do_something) (char, void*, int), void* data)
{
  FILE* in = stdin;
  FILE* out = stdout;

  main_loop(&in, &out, do_something, data);
}

char do_nothing(char letter, void* data, int inc)
{
  return letter;
}

size_t count_char_stat(statistic_t* stat, char letter)
{
  assert(stat);
  assert( letter >= 'A' && letter <= 'Z');
  
  return stat->letter_count[(size_t)(letter - 'A')];
}

int fequals(float lvalue, float rvalue, float approx)
{
  return ( fapprox(lvalue, rvalue)  <= approx );
}

float fapprox(float lvalue, float rvalue)
{
  return fabs(lvalue - rvalue);
}

void mask_string(char* str, size_t size, size_t step)
{
  assert(str);
  assert(step > 0);

  if( step > size )
    {
      for(size_t i = 1; i<size; i++){ str[i] = ' '; }
      return;
    }
  
  size_t letter_count = 0;
  
  for(size_t i = 0; i<size; i++)
    {
      if(str[i] >= 'A' && str[i] <= 'Z')
	{
	  if( letter_count%step != 0 )
	    {
	      str[i] = ' ';
	    }

	  letter_count++;
	}
      else
	{
	  str[i] = ' ';
	}
    }
}

void mask_space(char* str, size_t size)
{
  assert(str);
  statistic_t stat;
  init_stat(&stat);
  make_stat_from_str(&stat, str, size);

  size_t space_independant = 0;

  for(size_t i = 0; i<size; i++)
    {
      if( str[i] != ' ' )
	{
	  str[space_independant] = str[i];
	  space_independant++;
	}
    }
  str[space_independant] = '\0';
}

float index_of_coicidence(char* str, size_t size)
{
  assert(str);
  
  float res = 0.0f;
  
  statistic_t stat;
  init_stat(&stat);
  make_stat_from_str(&stat, str, size);
  
  float total_count = stat.letter_total;
  float total_occ_res = (total_count * (total_count - 1));
  
  if( total_occ_res <= 0 ) { return -1.0; }
  
  for(char c = 'A'; c <= 'Z'; c++)
    {
      float c_count = (float)(count_char_stat(&stat, c));

      float letter_occ_res = (c_count * (c_count - 1));
      
      res += letter_occ_res/total_occ_res;
    }

  return res;
}
  

void partial_vigenere_decrypt(char* str, char* key, size_t n)
{
  partial_vigenere_decrypt_color(str, key, n, 0);
}

void partial_vigenere_decrypt_color(char* str, char* key, size_t n, int colored)
{
  assert(str);
  assert( strlen(key) > 0 );
  size_t str_sz = strlen(str);
  size_t sz = n;
  if( n > str_sz ){ n = str_sz; }
  size_t key_size = strlen(key);
  size_t letter_index = 0;
  
  for(size_t i=0; i<sz; i++)
    {
      if( str[i] == ' ' ){ printf(" "); }
      
      if( str[i] >= 'A' && str[i] <= 'Z' )
	{
	  int offset = ( key[letter_index%key_size] - 'A' );

	  // display one character in bold each five character
	  // in order to make the reading more easy (for counting indexes)	  
	  size_t const display_size = 256;
	  char display[display_size];
	  
	  unsigned int underline = 2;
	  if( i%5==4 ){ underline = 4; }
	  
	  sprintf(display, "\e[%zd;%dm", 31 + (i%key_size)%5, underline);

	  if( colored )
	    {
	      printf("%s", display);
	    }
	  
	  printf("%c", rotate_left(str[i], offset));
	  printf("\e[0m");
	  letter_index++;
	}
      else
	{
	  printf("%c", str[i]);
	}
    }
  printf("\n");

}

void get_a_key(char* str, char* key, size_t const key_size)
{
  assert(str);
  assert(key);
  
  mask_space(str, strlen(str));
  size_t const size = strlen(str);
  key[key_size] = '\0';

  char msg[key_size][size];

  for(size_t i = 0; i < key_size; i++)
    {
      // construct key_size messages blocs
      strcpy(msg[i], str + i);
      mask_string( msg[i], size - i, key_size);
      mask_space(msg[i], size - i);
      size_t msg_size = strlen(msg[i]);

      // find the E in the msgbloc
      statistic_t stat;
      init_stat(&stat);
      make_stat_from_str(&stat, msg[i], msg_size);

      char e = find_e_stat(&stat);
      int offset = e - 'E';
      while(offset < 0){ offset += ALPHA_NUM; }

      key[i] = (offset)%ALPHA_NUM + 'A';
    }
}


int key_size_compare_ioc(void const* k1, void const* k2)
{
  key_size_t* key1 = (key_size_t*)(k1);
  key_size_t* key2 = (key_size_t*)(k2);


  return (int)(10000*fapprox(key1->ioc, FRENCH_IOC)) - (int)(10000*fapprox(key2->ioc, FRENCH_IOC));
}

int key_size_compare_size(void const* k1, void const* k2)
{
  key_size_t* key1 = (key_size_t*)(k1);
  key_size_t* key2 = (key_size_t*)(k2);
  
  return key1->size - key2->size;
}

void get_key_size(key_size_t* keys, char* str, size_t const size)
{
  assert(str);
  assert(keys);
  
  for(size_t key_size = KEY_SIZE_MIN; key_size <= KEY_SIZE_MAX; key_size++)
    {
      char msg[size];
      strcpy(msg, str);
      mask_string(msg, size, key_size);
      mask_space(msg, size);
      size_t msg_size = strlen(msg);

      float ioc = index_of_coicidence(msg, msg_size);
      
      keys[key_size-1].ioc = ioc;
      keys[key_size-1].size = key_size;
    }

  qsort(keys, size, sizeof(key_size_t), &key_size_compare_ioc);
}


void make_char_unique(char* str, size_t size)
{
  long unsigned int visited = 0;
  size_t occ_independant = 0;
  
  for(size_t i=0; i<size; i++)
    {
      char c = str[i];
      unsigned offset = c - 'A'; 
      if( (visited & (1<<offset)) == 0 )
	{
	  visited |= 1<<offset;
	  *(str + occ_independant) = c;
	  occ_independant++;

	}
    }

  str[occ_independant] = '\0';
  printf("\n");
}

void substitution_init(substitution_t* self)
{
  assert(self);
  for(size_t letter=0; letter<ALPHA_NUM; letter++)
    {
      self->letters[letter] = (char)(letter + 'A');
    }
}

void substitution_fill_with_key(substitution_t* self, char* key, size_t key_size)
{
  assert(self);
  assert(key);
  
  char k[key_size];
  strncpy(k, key, key_size);
  make_char_unique(k, key_size);
  size_t k_size = strlen(k);

  memcpy(self->letters, k, k_size);

  char alpha[2] = {k[k_size-1], '\0'};
  
  for(size_t letter=k_size; letter<ALPHA_NUM; letter++)
    {
      while( strstr(k, alpha) != NULL )
	{
	  alpha[0]++;
	}
      
      self->letters[letter] = alpha[0];
      alpha[0]++;
      if( alpha[0] > 'Z' ){ alpha[0] = 'A'; }
    }
}

void substitution_dump(substitution_t *self)
{
  for(size_t i=0; i<ALPHA_NUM; i++)
    {
      printf("%c => %c\n", (char)(i+'A'), self->letters[i]);
    }
}

void substitution_i_dump(substitution_t *self)
{
  for(char c= 'A'; c!='Z'; c++)
    {

      int found = 0;
      
      for(size_t i=0; i<ALPHA_NUM; i++)
	{
	  if(self->letters[i] == c)
	    {
	      found = 1;
	      printf("%c <= %c\n", c, (char)(i + 'A'));
	      break;
	    }
	}

      if(found == 0)
	{
	  printf("%c <= \n", c);
	}
    }
}

char substitution_encrypt(char letter, void* subs, int itr)
{
  substitution_t* substitution = (substitution_t*)(subs);
  if( letter < 'A' || letter > 'Z' ){return letter;}
  return substitution->letters[letter - 'A'];
}

char substitution_decrypt(char letter, void* subs, int itr)
{
  substitution_t* substitution = (substitution_t*)(subs);
  if( letter < 'A' || letter > 'Z' ){return letter;}
  for(size_t i=0; i<ALPHA_NUM; i++)
    {
      if(substitution->letters[i] == letter)
	{
	  return i + 'A';
	}
    }
  return letter;
}



void get_string(char* str, size_t size)
{
  fgets(str, size, stdin);
  
  // ignore only \n
  while( str[0] == '\n')
    {
      fgets(str, size, stdin);
    }
      
  // remove \n at the end of the string
  for(size_t i=0; i<size;i++)
    {
      if(str[i]=='\n')
	{
	  str[i] = '\0';
	  break;
	}
    }
  
}

void vigenere_edit_key(char* key, size_t key_size, char* str)
{
  assert(key);

  
  size_t const usr_size = 256;
  char usr[usr_size];
  char *help = "?- to ask the wizard what character you must put in the key\nh- print the (H)elp\na- print the (A)lphabet\nk- print the (K)ey\nt- print the encrypted (T)ext\nq- (Q)uit the edit mode";

  printf("\n\n\n%s\n\n", help);
  printf("-> Edit key ");
  print_colored_key(key, key_size);
  printf("\n");
  
  while( 1 )
    {
      printf("> ");
      get_string(usr, usr_size);
      
      // quit
      if(strcmp(usr, "q") == 0){ return; }

      // alphabet
      if(strcmp(usr, "a") == 0)
	{
	  for(char l = 'A'; l <= 'Z'; l++)
	    {
	      printf("%c ", l);
	    }
	  printf("\n");
	}

      // compute letter offset
      if(strcmp(usr, "?") == 0)
	{
	  printf("from : ");
	  get_string(usr, usr_size);
	  char from = usr[0];

	  printf("\nto : ");
	  get_string(usr, usr_size);
	  char to = usr[0];

	  printf("\nwith the key : ");
	  get_string(usr, usr_size);
	  char with = usr[0];

	  int wants_offset = to - from;
	  int letter_offset = ((with - 'A') - wants_offset)%ALPHA_NUM;
	  while( letter_offset < 0 ){ letter_offset+=ALPHA_NUM; }
	  char new_letter = 'A' + letter_offset;
	  
	  printf("to get a %c with a %c, you need a key of %c\n", to, from, new_letter);
	  printf("\n");
	}
      
      // change a letter
      size_t index = atoi(usr);
      if( index >= 1 && index <= key_size+1 )
	{
	  printf("change number %zd into : ", index);
	  get_string(usr, usr_size);
	  key[index-1] = usr[0];

	  printf("New key : ");
	  for(size_t i=0; i<key_size;i++)
	    {
	      printf("\e[%zdm%c", 31 + (i%5), key[i]);
	    }
	  printf("\e[0m\n");
	  
	  partial_vigenere_decrypt_color(str, key, 200, 1);
	}
      
      // print key
      if( strcmp(usr, "k") == 0 )
	{
	  printf("key :");
	  print_colored_key(key, key_size);
	  printf("\n");
	}

      // print text
      if( strcmp(usr, "t") == 0 )
	{
	  printf("text :");
	  partial_vigenere_decrypt_color(str, key, 200, 1);
	}

      // help
      if( strcmp(usr, "h") == 0 )
	{
	  printf("%s\n", help);
	}
    }
}

void print_colored_key(char* key, size_t key_size)
{
  for(size_t i=0; i<key_size;i++)
    {
      unsigned int underline = 2;
      if( i%5==4 ){ underline = 4; }
      printf("\e[%zd;%dm%c", 31 + (i%5), underline,key[i]);
      printf("\e[0m");
    }
}


char caesar_encrypt (char letter, void* data, int itr)
{
  int* offset = (int*)(data);
  return rotate_right(letter, *offset);
}

char caesar_decrypt(char letter, void* data, int itr)
{
  int* offset = (int*)(data);
  return rotate_left(letter, *offset);
}

char vigenere_encrypt(char letter, void* data, int inc)
{
  char* key = (char *) data;
  size_t size = strlen(key);
  char keychar = key[inc%size];
  int offset = (int)('A' - keychar);

  return rotate_left(letter, offset);
}

char vigenere_decrypt(char letter, void* data, int inc)
{
  if(letter < 'A' || letter > 'Z') return letter;
  
  char* key = (char *) data;
  size_t size = strlen(key);
  char keychar = key[inc%size];
  int offset = (int)('A' - keychar);
  
  return rotate_right(letter, offset);
}

void substitution_print_menu()
{
  printf("o- print the (O)riginal text\n");
  printf("p- (P)rint the text\n");
  printf("t- print the substitution (T)able\n");
  printf("i- print the (I)nverted substitution table\n");
  printf("a- print st(A)tistics\n");
  printf("w- Make the (W)izard find some letters\n");
  printf("r- (R)eplace a letter by another\n");
  printf("k- (K)ill a letter\n");
  printf("d- (D)elete buffer and reset with original text value\n");
  printf("h- print (H)elp\n");
  printf("s- (S)ave the buffer\n");
  printf("l- (L)oad the buffer\n");
  printf("e- (E)xport translation\n");
  printf("q- (Q)uit\n");

  printf("\ncolors : normal\e[32m your modification \e[31mconflicts \e[33m wizard modification\n");
  printf("\e[0m\n");
}

int substitution_get_action()
{
  char action;
  fread(&action, sizeof(char), 1, stdin);
  if( action >= 'A' && action <= 'Z' ){ action += ('a' - 'A'); }
  if( action < 'a' || action > 'z' ){ return E_SUBSTITUTION_CONTINUE; }
  
  switch(action)
    {
    case 'o':
      printf("-> print original text\n");
      return E_SUBSTITUTION_ORIGINAL;
      break;
    case 'p':
      printf("-> print text\n");
      return E_SUBSTITUTION_PRINT;
      break;
    case 't':
      printf("-> print substitution table\n");
      return E_SUBSTITUTION_TABLE;
      break;
    case 'i':
      printf("-> print inverted substitution table\n");
      return E_SUBSTITUTION_INVERTED_TABLE;
      break;
    case 'a':
      printf("-> print statistics\n");
      return E_SUBSTITUTION_STAT;
      break;
    case 'w':
      printf("-> ~WiZaRd~ !\n");
      return E_SUBSTITUTION_WIZARD;
      break;
    case 'r':
      printf("-> swap letter\n");
      return E_SUBSTITUTION_SWAP;
      break;
    case 'k':
      printf("-> kill letter\n");
      return E_SUBSTITUTION_KILL;
      break;
    case 'd':
      printf("-> delete buffer\n");
      return E_SUBSTITUTION_DELETE;
      break;
    case 'h':
      printf("-> help\n");
      return E_SUBSTITUTION_HELP;
      break;
    case 's':
      printf("-> save buffer\n");
      return E_SUBSTITUTION_SAVE;
    case 'l':
      printf("-> load buffer\n");
      return E_SUBSTITUTION_LOAD;
    case 'e':
      printf("-> exported\n");
      return E_SUBSTITUTION_EXPORT;
      break;
    case 'q':
      printf("-> quit\n");
      return E_SUBSTITUTION_QUIT;
      break;

    default: printf("unknown entry menu : %c\n", action); break;
    }
  
  return E_SUBSTITUTION_CONTINUE;
}

void substitution_wizard(substitution_t* sub, statistic_t* stat, char* text, int* letters_is_original)
{
  assert(sub);
  assert(stat);
  
  // find q
  size_t const size = strlen(text);
  size_t const env_size = 10;
  char env[env_size];

  for(size_t i=0; i<size - env_size; i++)
    {
      for(size_t k = 0; k<env_size; k++)
      	{
      	  env[k] = text[i+k];
      	}
      
      // we have found _QU' !
      // _QU'
      if( env[0] == ' '
	  && isupper(env[1])
	  && isupper(env[2])
	  && env[3] == '\'')
	{
	  substitution_swap(sub, env[1], 'Q', letters_is_original);
	  letters_is_original[env[1] - 'A'] = 2;

	  substitution_swap(sub, env[2], 'U', letters_is_original);
	  letters_is_original[env[2] - 'A'] = 2;
	}      
    
    }

    // find e
  char e = find_e_stat(stat);

  substitution_swap(sub, e, 'E', letters_is_original);
  letters_is_original[e - 'A'] = 2;
  
}

void substitution_swap(substitution_t* sub, char letter1, char letter2, int* letters_is_original)
{
  if( isupper(letter1) )
    {
      sub->letters[letter1 - 'A'] = letter2;
      letters_is_original[letter1 - 'A'] = 1;
    }
}

void substitution_apply(substitution_t* sub, char* str, size_t size)
{
  assert(str);
  
  for(size_t i=0; i<size; i++)
    {
      if( isupper(str[i]) )
	{
	  str[i] = sub->letters[ str[i] - 'A' ];
	}
    }
}

void substitution_get_swap(substitution_t* sub, int* letters_is_original)
{
  char l1 = '!';
  printf("Lettre 1\n");
  while( (fread(&l1, sizeof(char), 1, stdin)) &&
	 (!isalpha(l1)) ){}

  char l2 = '!';
  printf("Lettre 2\n");
  while( (fread(&l2, sizeof(char), 1, stdin)) &&
	 (!isalpha(l2)) ){}

  substitution_swap(sub, l1, l2, letters_is_original);
}

void substitution_get_kill(substitution_t* sub, int* letters_is_original)
{
  char l = '!';
  printf("Lettre\n");
  while( (fread(&l, sizeof(char), 1, stdin)) &&
	 (!isalpha(l)) ){}

  substitution_kill(sub, l, letters_is_original);
}

void substitution_kill(substitution_t* sub, char l, int* letters_is_original)
{
  printf("%c killed !\n", l);
  
  if( isupper(l) )
    {
      letters_is_original[l - 'A'] = 0;
      sub->letters[l - 'A'] = '\0';
    }
}

void substitution_make_action(substitution_t* sub, statistic_t* stat, char* text, char* buffer, size_t size, int action, int* letters_is_original)
{
  switch(action)
    {
    case E_SUBSTITUTION_DELETE:
      {
	memcpy(buffer, text, size);
	substitution_init(sub);
	for(size_t i=0; i<ALPHA_NUM;i++)
	  {
	    letters_is_original[i] = 0;
	    sub->letters[i] = ' ';
	  }
      }
      break;

    case E_SUBSTITUTION_HELP:
      {
	substitution_print_menu();
      }
      break;
      
    case E_SUBSTITUTION_ORIGINAL:
      {
	fwrite(text, sizeof(char), size, stdout);
	fwrite("\n", sizeof(char), 1, stdout);
      }
      break;

    case E_SUBSTITUTION_PRINT:
      {
	for(size_t i=0; i<size;i++)
	  {
	    size_t const code_size = 32;
	    char code[code_size];
	    sprintf(code, "\e[0m");
	    char encrypted_letter = text[i];
	  
	    if( isupper(text[i]) )
	      {
		char replacement = sub->letters[text[i] - 'A'];
		if( isupper(replacement) )
		  {
		    encrypted_letter = sub->letters[text[i] - 'A'];
		  }
	      
		switch(letters_is_original[ text[i] - 'A' ])
		  {
		  case 1: sprintf(code, "\e[32m"); break; // found by the user
		  case 2: sprintf(code, "\e[33m"); break; // found by the wizard
		  case 4: sprintf(code, "\e[35m"); break; // not sure
		  default: sprintf(code, "\e[0m"); break;
		  }
	      
		int others = 0;
		for(size_t other_sub = 0; other_sub<ALPHA_NUM; other_sub++)
		  {
		    if(others>=2){break;}
		    if( sub->letters[other_sub] == encrypted_letter)
		      {
			others++;
		      }
		  }
		if(others >= 2)
		  {
		    sprintf(code, "\e[31m");
		  }
	      }
	  
	    printf("%s%c", code, encrypted_letter);
	    printf("\e[0m");
	  
	  }
	printf("\n");
      }
      break;

    case E_SUBSTITUTION_TABLE:
      {
	substitution_dump(sub);
      }
      break;

    case E_SUBSTITUTION_INVERTED_TABLE:
      {
	substitution_i_dump(sub);
      }
      break;

    case E_SUBSTITUTION_STAT:
      {
	dump_stat(stat);
      }
      break;
      
    case E_SUBSTITUTION_SWAP:
      {
	substitution_get_swap(sub, letters_is_original);
      }
      break;
      
    case E_SUBSTITUTION_KILL:
      {
	substitution_get_kill(sub, letters_is_original);
      }
      break;

    case E_SUBSTITUTION_SAVE:
      {
	FILE* buffer_file;
	buffer_file = fopen("substitution_break_save.txt","w+");
	  
	if( buffer_file != NULL )
	  {
	    fwrite(sub, sizeof(*sub), 1, buffer_file);
	    printf("Buffer saved !\n");
	    fclose(buffer_file);
	  }
	else
	  {
	    perror("Can't save the buffer");
	  }
      }
      break;

    case E_SUBSTITUTION_LOAD:
      {
	FILE* buffer_file;
	buffer_file = fopen("substitution_break_save.txt","r");
	  
	if( buffer_file != NULL )
	  {
	    fread(sub, sizeof(*sub), 1, buffer_file);
	    for(size_t i=0; i<ALPHA_NUM;i++)
	      {
		letters_is_original[i] = (sub->letters[i] != ' ');
	      }
	    printf("Buffer loaded !\n");
	    fclose(buffer_file);
	  }
	else
	  {
	    perror("Can't load the buffer");
	  }
      }
      break;

    case E_SUBSTITUTION_WIZARD:
      {
	substitution_wizard(sub, stat, text, letters_is_original);
      }
      break;

    case E_SUBSTITUTION_EXPORT:
    {
      FILE* export;
      export = fopen("substitution_break_out.txt", "w+");
      if( export == NULL )
	{
	  perror("can't export");
	  exit(EXIT_FAILURE);
	}

      for(size_t i=0; i<size; i++)
	{
	  char l = text[i];
	  
	  if( isupper(text[i]) )
	    {
	      l = sub->letters[text[i] - 'A'];
	    }
	  
	  fwrite(&l, sizeof(char), 1, export);
	}
      
      printf("export done !");
      fclose(export);
      
    }
    break;
    
    default:break;
    }
  
}
